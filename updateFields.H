  
  dEps = symm(fvc::grad(dU));
  dEpsV = tr(dEps);
  dEpsD = dev(dEps);
  dSigma = 2.0*G*(dEpsD - dEpsPD) + K*I*(dEpsV - dEpsPV);

  U += dU; 
  Eps += dEps;  
  EpsV += dEpsV;
  EpsD += dEpsD;
  sigma = s + P*I;

  n = e/(1.0+e);
  r = s/P;
if (runTime.outputTime())
  {
     volScalarField sigmaEq
      (
       IOobject
       (
	"sigmaEq",
	runTime.timeName(),
	mesh,
	IOobject::NO_READ,
	IOobject::AUTO_WRITE
	),
       sqrt((3.0/2.0)*magSqr(s))
       );
      Info<< "Max sigmaEq = " << max(sigmaEq).value()
	<< endl;
   volScalarField pReal
      (
       IOobject
       (
	"pReal",
	runTime.timeName(),
	mesh,
	IOobject::NO_READ,
	IOobject::AUTO_WRITE
	),
       -p
       );
   volVectorField UReal
      (
       IOobject
       (
	"dUReal",
	runTime.timeName(),
	mesh,
	IOobject::NO_READ,
	IOobject::AUTO_WRITE
	),
       -U
       );
  runTime.write();
  }

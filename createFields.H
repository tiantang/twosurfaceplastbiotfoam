    Info<< "Reading pore pressure field\n" << endl;
    volScalarField p
    (
        IOobject
        (
            "p",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    volScalarField dp
    (
        IOobject
        (
            "dp",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
       dimensionedScalar("dp", dimPressure, 0)
    );

   Info<< "Reading displacement (increment) field\n" << endl;
   volVectorField dU
    (
        IOobject
        (
            "dU",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    volTensorField gradDU = fvc::grad(dU);

    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        dU
    );
  volTensorField gradU = fvc::grad(U);

    Info<< "Reading stress condition field\n" << endl;
    volSymmTensorField dSigma
    (
        IOobject
        (
            "dSigma",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("dSigma", dimForce/dimArea, symmTensor::zero)
    );

    volSymmTensorField s
    (
        IOobject
        (
            "s",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("s", dimForce/dimArea, symmTensor::zero)
    ); 

    volScalarField P
    (
        IOobject
        (
            "P",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
         mesh,
        dimensionedScalar("P", dimForce/dimArea, 0.0)
    ); 

     volSymmTensorField sigma
    (
        IOobject
        (
            "sigma",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        s+I*P
    );
    sigma.correctBoundaryConditions();

   volSymmTensorField r
    (
        IOobject
        (
            "r",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        s/P
    ); 

   Info<< "Reading internal state field\n" << endl;
    volScalarField m
    (
        IOobject
        (
            "m",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedScalar("m", dimless, m0.value())
    );

    volSymmTensorField alpha
    (
        IOobject
        (
            "alpha",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("alpha", dimless, symmTensor::zero)
    );

    Info<< "Reading fabric tensor field\n" << endl;
    volSymmTensorField F
    (
        IOobject
        (
            "F",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("F", dimless, symmTensor::zero)
    );

    Info<< "Creating strain field\n" << endl;
    volSymmTensorField dEps
    (
        IOobject
        (
            "dEps",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     symm(1.0/2.0*(gradDU+gradDU.T()))
    );
    
    volScalarField dEpsV
    (
        IOobject
        (
            "dEpsV",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
     tr(dEps)
    );

    volSymmTensorField dEpsD
    (
        IOobject
        (
            "dEpsD",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     dev(dEps)
    );

  /*  volSymmTensorField dEpsP
    (
        IOobject
        (
            "dEpsP",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedSymmTensor("dEpsP", dimless, symmTensor::zero)
    );
*/
    volScalarField dEpsPV
    (
        IOobject
        (
            "dEpsPV",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
         mesh,
        dimensionedScalar("dEpsPV", dimless, 0.0)
    );

    volSymmTensorField dEpsPD
    (
        IOobject
        (
            "dEpsPD",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
         mesh,
        dimensionedSymmTensor("dEpsPD", dimless, symmTensor::zero)
    );

    volSymmTensorField Eps
    (
        IOobject
        (
            "Eps",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
     symm(1.0/2.0*(gradU+gradU.T()))
    );
    
    volScalarField EpsV
    (
        IOobject
        (
            "EpsV",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
     tr(Eps)
    );

    volSymmTensorField EpsD
    (
        IOobject
        (
            "EpsD",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     dev(Eps)
    );  

    Info<< "Reading soil state\n" << endl;
    volScalarField e  //void ratio
    (
        IOobject
        (
            "e",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     mesh,
     dimensionedScalar("e", dimless, e0.value())
    ); 

    volScalarField n  //porosity
    (
        IOobject
        (
            "n",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     e/(1.0+e)
    );

  /*   volScalarField Kprime  //porosity
    (
        IOobject
        (
            "Kprime",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
     1.0/(1.0/Kw + (1.0-Sr)/(p0+b*p))
    );*/
   
    volScalarField Dp = (k/gamma*Kprime/n);
    volScalarField Dp2 = Kprime/n;

     volScalarField psi // state parameter
    (
        IOobject
        (
            "psi",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
      mesh,
     dimensionedScalar("psi", dimless, (e0-e_cref).value())
    ); 

    Info<< "Reading elastic moduli\n" << endl;
    volScalarField K
    (
        IOobject
        (
            "K",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
    mesh,
    dimensionedScalar("K", dimPressure, K0.value())
    // K0.value()*Foam::pow(P/P_at.value(),a.value())
    ); 
  
    K = K0*Foam::pow(P/P_at,a);

     volScalarField G
    (
        IOobject
        (
            "G",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
     mesh,
     dimensionedScalar("G", dimPressure, G0.value())
    // G0.value()*Foam::pow(P/P_at.value(),a.value())
    ); 
    G = G0*Foam::pow(P/P_at,a);

    surfaceScalarField Kf = fvc::interpolate(K, "K");
    surfaceScalarField Gf = fvc::interpolate(G, "G");
   
    Info<< "Creating explicit div dSigma field divDsigmaExp\n" << endl;
     volVectorField divDsigmaExp
    (
        IOobject
        (
            "divDsigmaExp",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh,
        dimensionedVector("divDsigmaExp", dimensionSet(1, -2, -2, 0, 0, 0, 0), vector::zero)
    );

     volScalarField yieldFlag
    (
        IOobject
        (
            "yieldFlag",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("yieldFlag", dimless, 0)
    );
